//Activity

fetch('https://jsonplaceholder.typicode.com/todos/1').then((response) => response.json()).then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos').then((response) => response.json()).then((json) => {

	let lists = json.map((todo => {
		return todo.title;
	}))

	console.log(lists);

});

fetch('https://jsonplaceholder.typicode.com/todos/1').then((response) => response.json()).then((json) => console.log(`The item "${json.title}" on the list has a status of ${json.completed}`));

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
  	'Content-type': 'application/json',
	},
	body: JSON.stringify({
	  	title: 'Create to do list items',
	  	completed: false,
	  	userId: 1
	})
}).then((response) => response.json()).then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
  	'Content-type': 'application/json',
	},
	body: JSON.stringify({
	  	title: 'Update to do list items',
	  	description: 'To update the my to do list with a different data structure.',
	  	status: 'Pending',
	  	dateCompleted: 'Pending',
	  	userId: 1
	})
}).then((response) => response.json()).then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
  	'Content-type': 'application/json',
	},
	body: JSON.stringify({
	  	status: 'Complete',
	  	dateCompleted: '01/11/23'
	})
}).then((response) => response.json()).then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'DELETE',
});